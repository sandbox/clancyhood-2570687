YNot
====

A simple YAML-based means of adding blocks, pages and tokens to Drupal.

Concisely, **YNot** allows you to use your favourite text editor to add HTML/PHP files to a folder and have their output available as blocks, pages or tokens (or all three) in the drupal interface.

##Y?

- So we can use our cool text editors and version control to create and track content.
- So we don't have to create content in dev that clashes with content in the production database - keep user content in core tables and admin content in files
- Because sticking full HTML, css or javascript in a regular page or block textarea is horrible
- Having some blocks available as tokens is so cool it seems a missing feature in itself

Brief example
-------------

The following defines both a block and a token.


	---
	name : An example ynot block
	description : An example block, also available as a token.
	tokenize : true
	block_info :  
		title : This will be the block's title as shown to the user, but tokens will just output the content below
	process : 
		text_format : full_html
	---

	<h2>Hello, world</h2>
	
	I am a block, but I'm also a token


Here's a quick page:


	---
	name : An example ynot page
	description : An example page, but also available as a block.
	menu : 
		path : path/to/this/page
	block_info : true
	process : 
		php : true
	---
	
	<?php print "Hello, world!"; ?>


Creating A YNot File
--------------------

By default, YNot files ("yfiles") belong in a directory called 'content' in your conf_path() folder (typically `sites/default/content` but varies on multi-site installations), or one called 'ynot' in your default theme directory. Modules that include their own yfiles may put them anywhere they like.

A file should end in `.inc` and have a grokkable and descriptive name, like `front-page-copyright-notice.inc`. Best practice is to stick to lowercase ascii and single dashes (since filenames will be sanitized this way anyway for internal addressing and token naming purposes). 

After editing the YAML header of a file you must **clear caches**, but the content part of the file is available immediately unless you've enabled the YNot content cache option on in `admin/config/development/performance` (best for production). The processed output of a YNot file will never be cached unless you enable the various cacheing options for blocks and pages within drupal itself or in the config options of hook_block_info and hook_menu set by the yfile.


YAML Options
------------

You may if you like omit the YAML header entirely, and the HTML within the file will simply be made available as a block, like so:

	Hello world! This block has no header, and is just plain <b>HTML</b>.

Otherwise, the options are as follows:

#### name, description
The name and description of the item as used in the block and token admin interfaces.

#### block_info
`true/false`, or an array of options to pass to [hook_block_info](https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_info/7) - by default, `block_info.info` is the same as `name`. Defaults `true` where a YAML header is missing entirely, `false` otherwise.

#### block_view
An array of options to add to the renderable block itself, as per [hook_block_view](https://api.drupal.org/api/drupal/modules%21block%21block.api.php/function/hook_block_view/7) - additionally, if `process.token_filter` is true then YNot will attempt to parse any tokens found in `block_view.subject`. Oh yes, the default of `block_view.subject` is `<none>` so you may omit it without fear of unwanted headings. 

#### menu_path
A path via which the file may be accessed as a drupal page, as per hook_menu. You may of course use wildcards or entity loaders such as `menu_path : mydir/%node`, (providing you also set e.g. `menu.page arguments : [1]`) and you will find these entities available within the yfile. Any other wildcard arguments (that don't invoke entity_load) will be made available within the variable $arguments.

#### menu
An array of options to pass to hook_menu as per [hook_menu](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7) - additionally `menu_path` must be set. 

Unlike hook_menu, `menu.access callback` defaults to true so you will need to define access options if they require credentials. 

Constants used by hook_menu are also available as normal either on their own or piped, for example:

	menu : 
		context : MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE
		
If `process.token_filter` is true then YNot will attempt to parse any tokens found in `menu.title`, e.g. `title: [node:title]`.

#### tokenize
`true/false` whether to make the yfile available as a token. The address of the block will be `[ynot:<ybucket>:<yfile>:block]` e.g. `sites/default/content/somedir/my-ynot-file.inc` = `[ynot:site:somedir--my-ynot-file:block]` (although this can be overwritten by `address`).

#### address
Overwrite the internal address of a file so it's nicer for tokenize. Whatever you set here will be used as the `<yfile>` portion of a file's address instead of the full path to the file within the bucket.

#### process
An array of things to do to the output. Should be in the order in which you want the processing to take place, but PHP will always come first. The built-in options are:

- `php : true/false` parse the php in the file ("include" it). Defaults false.
- `token_filter : true/false` parse tokens in the output, and the `menu.title` or `block_view.subject`.  YNot will always pass the global `$user` to token_replace if it is not already available in the current context, and also any entities loaded by menu. Tokens are only included as an option for mild convenience and if in doubt you should set `process.php` and use that to manipulate output (since no user should be accessing your filesystem anyway).
- `text_format : full_html/filtered_html/whatever` the machine_name of a text format you want the output to be processed by. Handy for adding line breaks if you're too lazy to write `<p>`

#### attached
An array of assets to attach to a block or page. Tokens may be used to get e.g. the path to the directory the YFile is in. Example use:

	attached:
	  js:
	    - //d3js.org/d3.v3.min.js
	    - [yfile:path]/somefile.js
	    - [yfile:bucket-path]/someotherfile.js
	 css: [[yfile:path]/somefile.css]
	 
The appearance of tokens within the YAML can be confusing to read, but tested YAML parsers seem to be robust enough to deal with it.
	    
#### ctools_category
The category to send to hook_ctools_block_info, for the sake of keeping Panels interface tidier. Defaults to 'YNot [<bucket>]'.
	    
	    
Token Properties
----------------

Any `tokenize`d YNot file makes available the following properties:
 
- `name`: The administrative name of the YFile
- `description` : the administrative description
- `markup` : the rendered output of the yfile
- `block` : the rendered output of the yfile as a block
- `uri` : the absolute URI to the YFile's parent directory. Handy for referencing assets.
- `bucket-uri` : the absolute URI to the bucket
- `path` : the drupal path to the YFile's parent directory. Handy for referencing assets using the attached config option.
- `bucket-path` : the drupal path to the bucket
- `source` : the source code of the YFile in the cache for demonstrative purposes - but files that contain PHP never expose their contents.
