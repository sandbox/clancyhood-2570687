---
name : An example ynot page
description : An example page, also available as a block.
block_info : true
menu_path : ynot-example/%node/an-example-page
menu :
	title : An example ynot page
	page arguments : [1]
	access callback : true
process : 
	php : true
	token_filter : true
	text_format : full_html
attached:
  css:
    - [yfile:path]/somefile2.css
  js:
    - //d3js.org/d3.v3.min.js
    - whatever.js
    - [yfile:path]/somefile.js
    - [yfile:bucket-path]/someotherfile.js
  
---

<h2><?php print "Hello, world! This is a page"; ?></h2>

<?php if ($yfile->process('text_format')) : ?>
Here's a line break added by the text format <b><?php echo $yfile->process('text_format'); ?></b>.
<?php endif; ?>

<?php if (isset($node)) : ?>
<p>Here's the title of a node (whose nid is set in the page path): <b><?php print check_plain($node->title); ?></b></p>
<?php endif; ?>

<h3>Here's a YNot block added as a token (ynot:ynot-example:example-block:block) <i>within</i> the YNot file:</h3>

<blockquote>
[ynot:ynot-example:example-block:block]
</blockquote>
