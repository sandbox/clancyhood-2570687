---
name : An example ynot block
description : An example block, also available as a token.
block_info : true
tokenize : true
block_output :  
	title : Hello "[user:name]"
process : 
	php : true
	token_filter : true
	text_format : full_html
---

<?php print "Hello, world! This YNot block contains PHP."; ?>

<?php if ($yfile->process('text_format')) : ?>
  Here's a line break added by the text format <b><?php echo $yfile->process('text_format'); ?></b>.
<?php endif; ?>
