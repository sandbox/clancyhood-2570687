---
name: Some markdown stuff
---

So here's some **markdown**.

We can put what we like here so let's think of some stuff.

-----------

[ynot:ynot-markdown-example:a-nice-map:markup]

-----------

Umm, a sort of timer thing:

<h3 id="c">0</h3>
<script>
(function($){
  var n = 0, $c = $('#c');
  setInterval(function(){ 
    $c.html(++n); }, 
  1000);
}(jQuery));
</script>

------------

Here's a link to [another file](another-file)

------------

You know what, I don't know what to tell you; it's just straight-up markdown so knock yourselves out. See the module file for pointers how to do this. Oh and here's the source of this file:

<pre style="background:#f6f6f6; padding: 2em"><code>[yfile:source]</code></pre>

--------------

Have fun.
