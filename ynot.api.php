<?php
/**
 * @file
 * Ynot API.
 */

/**
 * Define a 'bucket', or namespace for a set of YFiles.
 */
function hook_ynot_bucket_info() {

  $buckets = array();

  $buckets['site'] = array(
    'name' => 'Site content directory',
    'path' => conf_path() . '/content',
    'defaults' => array(),
  );

  // The line: 'defaults' => array(), sets default config for all files
  // within the bucket, for deep array merging.
  return $buckets;
}

/**
 * Define one or a number of process names and callbacks.
 *
 * Callback should take argument which is the value of the process.
 */
function hook_ynot_process_info() {
  return array(
    'text_format'  => 'ynot_process_text_format',
    'token_filter' => 'ynot_process_token_filter',
  );
}

/**
 * Affect the content array of a ynot file after processing (before rendering).
 */
function hook_ynot_view(&$content, &$variables) {

  if ($variables['yfile']->process('token_filter')) {

    switch ($variables['context']) {

      case 'menu':
        $menu_info = (array) $variables['yfile']->config('menu_info');
        if (isset($menu_info['title']) && !isset($menu_info['title callback'])) {
          drupal_set_title(token_replace($menu_info['title'], $variables));
        }
        break;

      case 'block':
        if ($variables['block']['subject'] && ($variables['block']['subject'] != '<none>')) {
          $variables['block']['subject'] = token_replace($variables['block']['subject'], $variables);
        }
        break;
    }

    if ($content['#attached']) {
      foreach ($content['#attached'] as $type => &$files) {
        foreach ($files as $key => &$value) {
          if (!is_array($value)) {
            $value = token_replace($value, $variables);
          }
        }
      }
    }
  }
}
